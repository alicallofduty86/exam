<?php
require_once './lib/checkAdmin.php';
?>
<html lang="en">

<head>
    <link rel="stylesheet" href="assets/css/pace.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Add Questions</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>
<style>
    .questions {
        display: none;
    }

    #addQ {
        margin-bottom: 20px;
    }
</style>
<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header text-center">
                    <h1 style="color:#fff"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
                  </div>
    <!-- /.navbar-right -->
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
        <aside class="site-sidebar clearfix">
                <nav class="sidebar-nav">
                        <ul class="nav in side-menu">
                          <li class="current-page menu-item-has-children"><a href="widgets-statistics1.html"><span class="color-color-scheme"></span><i class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Add Questions <span class="badge badge-border bg-primary pull-right">5</span></span></span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="results.php"><i class="list-icon material-icons">view_headline</i> <span class="hide-menu">View Results</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">contact_mail</i> <span class="hide-menu">Contact</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">info</i> <span class="hide-menu">About Us</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="./lib/logout.php"><i class="list-icon material-icons">exit_to_app</i> <span class="hide-menu">LogOut</span></a>

                            </li>
                        </ul>
                        <!-- /.side-menu -->
                    </nav>
            <!-- /.sidebar-nav -->
        </aside>
        <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <h1 class="text-center">Add Questions</h1>

            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body">
                                    <div class="form-group">
                                    <select class="btn  btn-lg btn-color-scheme" style="border: 1px solid #ccc;width:49%;" lev="0">
                                                    <option value="0">Select Level</option>
                                                    <option value="1">Beginner </option>
                                                    <option value="2">Upper Intermediate</option>
                                                    <option value="3">Intermediate</option>
                                                    <option value="4">Advance </option>
                                                  </select>
                                            <select class="btn  btn-lg btn-primary" style="border: 1px solid #ccc;width:49%;float:right" act="0">
                                                    <option value="0">Select Action</option>
                                                    <option value="1">Add Passage </option>
                                                    <option value="2">Add Questions To Passage</option>
                                                    <option value="3">Add Questions To Listener </option>
                                                    <option value="4">Add Vocabulary Questions </option>
                                                    <option value="5">Add Grammer Questions</option>
                                                  </select>
                                                  <br>
                                                  <br>
                                                  <select class="btn btn-block btn-lg" style="border: 1px solid #ccc;display:none;" op="0">

                                                  </select>
                                        </div>
                                 
                                 <div class="form-group" style="display:none" mn="0">
                                        <h5 class="box-title mr-b-0">Main Question</h5>
                                        <br>
                                        <textarea rows="5" style="width: 100%;" main="0"></textarea>
                                        <br>
                                        <h5 class="box-title mr-b-0">Title</h5>
                                        <br>
                                        <textarea rows="2" style="width: 100%" ti="0"></textarea>
                                 </div>

                                <br>
                               <!-- <button class="btn btn-info" id="addQ"> Add Questions</button> -->

                                <br>
                                <div class="questions" style="display:none" >
                                    <div class="question">
                                            <select id="examDegree" class="text-center pull-right py-1 px-2  btn btn-info ripple" dg="0">
                                                    <option value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                  </select>
                                        <h5 class="box-title mr-b-0">Question 1</h5>
                                        <br>
                                        <textarea rows="2" style="width: 100%" q="0"></textarea>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-2">
                                            <label>first answer</label>
                                            </div>
                                            <div class="col-md-8">
                                                    <input class="form-control" id="l0" placeholder="answer text" type="text" anss="0">
                                                </div>
                                                <div class="col-md-2">
                                                        <div class="radiobox radio-info">
                                                                <label>
                                                                        <input type="radio" name="t" value="1"  t="0"> <span class="label-text">true answer</span>
                                                                </label>
                                                            </div>
                                                    </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                                <div class="col-md-2">
                                                <label>second answer</label>
                                                </div>
                                                <div class="col-md-8">
                                                        <input class="form-control" id="l0" placeholder="answer text" type="text" anss1="0">
                                                    </div>
                                                    <div class="col-md-2">
                                                            <div class="radiobox radio-info">
                                                                    <label>
                                                                        <input type="radio" name="t" value="2" t="0"> <span class="label-text" >true answer</span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                            </div>
                                        <br>
                                        <div class="row">
                                                <div class="col-md-2">
                                                <label>third answer</label>
                                                </div>
                                                <div class="col-md-8">
                                                        <input class="form-control" id="l0" placeholder="answer text" type="text" anss2="0">
                                                    </div>
                                                    <div class="col-md-2">
                                                            <div class="radiobox radio-info">
                                                                    <label>
                                                                            <input type="radio" name="t" value="3" t="0"> <span class="label-text" >true answer</span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                            </div>
                                        <br>
                                        <div class="row">
                                                <div class="col-md-2">
                                                <label>fourth answer</label>
                                                </div>
                                                <div class="col-md-8">
                                                        <input class="form-control" id="l0" placeholder="answer text" type="text" anss3="0">
                                                    </div>
                                                    <div class="col-md-2">
                                                            <div class="radiobox radio-info">
                                                                    <label>
                                                                            <input type="radio" name="t" value="4" t="0"> <span class="label-text">true answer</span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                            </div>


                                    </div>
                                    <div class="question">
                                            <select id="examDegree" class="text-center pull-right py-1 px-2 btn btn-info ripple" name="" dg="1">
                                            <option value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                  </select>
                                            <h5 class="box-title mr-b-0">Question 2</h5>
                                            <br>
                                            <textarea rows="2" style="width: 100%" q="1"></textarea>
                                            <br>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-2">
                                                <label>first answer</label>
                                                </div>
                                                <div class="col-md-8">
                                                        <input class="form-control" id="l0" placeholder="answer text" type="text" anss="1">
                                                    </div>
                                                    <div class="col-md-2">
                                                            <div class="radiobox radio-info">
                                                                    <label>
                                                                            <input type="radio" name="t1" value="1" t1="1"> <span class="label-text">true answer</span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                    <div class="col-md-2">
                                                    <label>second answer</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                            <input class="form-control" id="l0" placeholder="answer text" type="text" anss1="1">
                                                        </div>
                                                        <div class="col-md-2">
                                                                <div class="radiobox radio-info">
                                                                        <label>
                                                                            <input type="radio" name="t1" value="2" t1="1"> <span class="label-text">true answer</span>
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                </div>
                                            <br>
                                            <div class="row">
                                                    <div class="col-md-2">
                                                    <label>third answer</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                            <input class="form-control" id="l0" placeholder="answer text" type="text" anss2="1">
                                                        </div>
                                                        <div class="col-md-2">
                                                                <div class="radiobox radio-info">
                                                                        <label>
                                                                                <input type="radio" name="t1" value="3" t1="1"> <span class="label-text">true answer</span>
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                </div>
                                            <br>
                                            <div class="row">
                                                    <div class="col-md-2">
                                                    <label>fourth answer</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                            <input class="form-control" id="l0" placeholder="answer text" type="text" anss3="1">
                                                        </div>
                                                        <div class="col-md-2">
                                                                <div class="radiobox radio-info">
                                                                        <label>
                                                                                <input type="radio" name="t1" value="4" t1="1"> <span class="label-text">true answer</span>
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                </div>

                                        </div>
                                        <div class="question">
                                                <select id="examDegree" class="text-center pull-right py-1 px-2 btn btn-info ripple" name="" dg="2">
                                                <option value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                      </select>
                                                <h5 class="box-title mr-b-0">Question 3</h5>
                                                <br>
                                                <textarea rows="2" style="width: 100%" q="2"></textarea>
                                                <br>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                    <label>first answer</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                            <input class="form-control" id="l0" placeholder="answer text" type="text" anss="2">
                                                        </div>
                                                        <div class="col-md-2">
                                                                <div class="radiobox radio-info">
                                                                        <label>
                                                                                <input type="radio" name="t2" value="1" t2="2"> <span class="label-text">true answer</span>
                                                                        </label>
                                                                    </div>
                                                            </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                        <div class="col-md-2">
                                                        <label>second answer</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                                <input class="form-control" id="l0" placeholder="answer text" type="text" anss1="2">
                                                            </div>
                                                            <div class="col-md-2">
                                                                    <div class="radiobox radio-info">
                                                                            <label>
                                                                                <input type="radio" name="t2" value="2" t2="2"> <span class="label-text">true answer</span>
                                                                            </label>
                                                                        </div>
                                                                </div>
                                                    </div>
                                                <br>
                                                <div class="row">
                                                        <div class="col-md-2">
                                                        <label>third answer</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                                <input class="form-control" id="l0" placeholder="answer text" type="text" anss2="2">
                                                            </div>
                                                            <div class="col-md-2">
                                                                    <div class="radiobox radio-info">
                                                                            <label>
                                                                                    <input type="radio" name="t2" value="3" t2="2"> <span class="label-text">true answer</span>
                                                                            </label>
                                                                        </div>
                                                                </div>
                                                    </div>
                                                <br>
                                                <div class="row">
                                                        <div class="col-md-2">
                                                        <label>fourth answer</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                                <input class="form-control" id="l0" placeholder="answer text" type="text" anss3="2">
                                                            </div>
                                                            <div class="col-md-2">
                                                                    <div class="radiobox radio-info">
                                                                            <label>
                                                                                    <input type="radio" name="t2" value="4" t2="2"> <span class="label-text">true answer</span>
                                                                            </label>
                                                                        </div>
                                                                </div>
                                                    </div>

                                            </div>
                                            <div class="question">
                                                    <select id="examDegree" class="text-center pull-right py-1 px-2 btn btn-info ripple" name="" dg="3">
                                                    <option value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                          </select>
                                                    <h5 class="box-title mr-b-0">Question 4</h5>
                                                    <br>
                                                    <textarea rows="2" style="width: 100%" q="3"></textarea>
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                        <label>first answer</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                                <input class="form-control" id="l0" placeholder="answer text" type="text" anss="3">
                                                            </div>
                                                            <div class="col-md-2">
                                                                    <div class="radiobox radio-info">
                                                                            <label>
                                                                                    <input type="radio" name="t3" value="1" t3="3"> <span class="label-text">true answer</span>
                                                                            </label>
                                                                        </div>
                                                                </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                            <div class="col-md-2">
                                                            <label>second answer</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                    <input class="form-control" id="l0" placeholder="answer text" type="text" anss1="3">
                                                                </div>
                                                                <div class="col-md-2">
                                                                        <div class="radiobox radio-info">
                                                                                <label>
                                                                                    <input type="radio" name="t3" value="2" t1="3"> <span class="label-text">true answer</span>
                                                                                </label>
                                                                            </div>
                                                                    </div>
                                                        </div>
                                                    <br>
                                                    <div class="row">
                                                            <div class="col-md-2">
                                                            <label>third answer</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                    <input class="form-control" id="l0" placeholder="answer text" type="text" anss2="3">
                                                                </div>
                                                                <div class="col-md-2">
                                                                        <div class="radiobox radio-info">
                                                                                <label>
                                                                                        <input type="radio" name="t3" value="3" t2="3"> <span class="label-text">true answer</span>
                                                                                </label>
                                                                            </div>
                                                                    </div>
                                                        </div>
                                                    <br>
                                                    <div class="row">
                                                            <div class="col-md-2">
                                                            <label>fourth answer</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                    <input class="form-control" id="l0" placeholder="answer text" type="text" anss3="3">
                                                                </div>
                                                                <div class="col-md-2">
                                                                        <div class="radiobox radio-info">
                                                                                <label>
                                                                                        <input type="radio" name="t3" value="4" t3="3"> <span class="label-text">true answer</span>
                                                                                </label>
                                                                            </div>
                                                                    </div>
                                                        </div>

                                                </div>
                                                <div class="question">
                                                      
                                                      
                                                        
                                                       
                                                      

                                                    </div>
                                                    <br>





                            </div>
                            <button class="btn btn-success ripple" save="1"><i class="material-icons list-icon">check</i>  <span>Save</span>
                                                        </button>

                                                        <div class="btn btn-success ripple" style="display:none" not="0">
                                                            
                                                        </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.col-md-12 -->
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
        <div class="swal2-container swal2-fade swal2-shown" style="overflow-y: auto;display:none;padding-top: 11%;" notf="0">
            <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show" tabindex="-1" style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 345px;">
            <ul class="swal2-progresssteps" style="display: none;">
            </ul>
            <div class="swal2-icon swal2-error" style="display: none;">
            <span class="swal2-x-mark">
            <span class="swal2-x-mark-line-left">
            </span>
            <span class="swal2-x-mark-line-right">
            </span>
            </span>
            </div>
            <div class="swal2-icon swal2-question" style="display: none;">?</div>
            <div class="swal2-icon swal2-warning" style="display: none;">!</div>
            <div class="swal2-icon swal2-info" style="display: none;">i</div>
            <div class="swal2-icon swal2-error swal2-animate-error-icon" style="display: block;">
            <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255);">
            </div>
            <span class="swal2-x-mark swal2-animate-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>
             <div class="swal2-success-ring"></div> 
             <div class="swal2-success-fix" style="background: rgb(255, 255, 255);"></div>
             <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255);"></div>
             </div><img class="swal2-image" style="display: none;"><h2 class="swal2-title" id="swal2-title">Error!!!</h2>
             <div id="swal2-content" class="swal2-content" style="display: block;">Error In Inserting Questions !</div>
             </div></div>
    </div>
    <!-- /.content-wrapper -->
    <div class="swal2-container swal2-fade swal2-shown" style="overflow-y: auto;display:none;padding-top: 11%" notf="1">
    <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show" tabindex="-1" style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 345px;">
            <ul class="swal2-progresssteps" style="display: none;"></ul>
            <div class="swal2-icon swal2-error" style="display: none;">
                    <span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span>
                    <span class="swal2-x-mark-line-right"></span>
                </span>
        </div>
        <div class="swal2-icon swal2-question" style="display: none;">?</div>
        <div class="swal2-icon swal2-warning" style="display: none;">!</div
        ><div class="swal2-icon swal2-info" style="display: none;">i</div>
        <div class="swal2-icon swal2-success swal2-animate-success-icon" style="display: block;">
                <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255);"></div>
                <span class="swal2-success-line-tip swal2-animate-success-line-tip"></span> 
                <span class="swal2-success-line-long swal2-animate-success-line-long"></span>
                <div class="swal2-success-ring"></div> <div class="swal2-success-fix" style="background: rgb(255, 255, 255);"></div>
                <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255);"></div></div>
                <img class="swal2-image" style="display: none;">
                <h2 class="swal2-title" id="swal2-title">Done</h2>
                <div id="swal2-content" class="swal2-content" style="display: block;">Information Added Successfully</div>
                <input class="swal2-input" style="display: none;">
                <input type="file" class="swal2-file" style="display: none;">
                <div class="swal2-range" style="display: none;"><output></output><input type="range"></div>
                <select class="swal2-select" style="display: none;"></select><div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;">
                        <input type="checkbox"></label><textarea class="swal2-textarea" style="display: none;"></textarea>
                        <div class="swal2-validationerror" style="display: none;"></div>
                </div>
</div>
    <!-- FOOTER -->
    <footer class="footer text-center clearfix">2017 © Oscar Admin brought to you by UnifatoThemes</footer>
    </div>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/themes/inlite/theme.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/jquery.tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/custom.js"></script>

    <script>

            $('[act="0"]').change(function(){
                var val = $('[act="0"]').val();
                console.log(val);
                if(val == 0){
                    $('[mn="0"]').css("display", "none");
                    $('.questions').css("display", "none");
                    $('[op="0"]').css("display", "none");
                        }else if(val == 1){
                        $('[mn="0"]').css("display", "inline");
                        $('.questions').css("display", "none");
                        $('[op="0"]').css("display", "none");
                        }else if(val == 2){
                            $('[mn="0"]').css("display", "none");
                            getMainQuestions(3);
                            $('.questions').css("display", "inline");
                        }else if(val == 3){
                            $('[mn="0"]').css("display", "none");
                            getMainQuestions(4);
                            $('.questions').css("display", "inline");
                        }else if(val == 4){
                            $('.questions').css("display", "inline");
                            $('[op="0"]').css("display", "none");
                        }else if(val == 5){
                            $('.questions').css("display", "inline");
                            $('[op="0"]').css("display", "none");
                        }else{
                              // must select
                        }
            });

            function getMainQuestions(val){
                $(`[op="0"]`).html('');
                var data = "id=" + val;
                var xhr = new XMLHttpRequest();
                xhr.withCredentials = true;
                xhr.addEventListener("readystatechange", function () {
                if (this.readyState === 4) {
                    $('[op="0"]').css("display", "inline");
                    var myArr = JSON.parse(this.responseText);
                    console.log(myArr.data);
                    for(var i = 0;i < myArr.data.length;i++){
                        
                            var temp = `<option value="`+ myArr.data[i].id +`">`+ myArr.data[i].title +`</option>`;
                            $(`[op="0"]`).append(temp);
                        
                    }
                }
                });
                xhr.open("POST", "http://localhost/exam/service.php?function=getMainQuestions");
                xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
                xhr.send(data);
               }
               function createMainQuestions(si){
                var ti = $('[ti="0"]').val();
                var main = $('[main="0"]').val();
                var li = $(`[lev="0"]`).val();
                //var si = $('[act="0"]').val();
                var data = "title=" +ti+ "&MainQuestion=" + main+ "&sectionId="+ si + "&levelId=" + li;
                var xhr = new XMLHttpRequest();
                xhr.withCredentials = true;
                xhr.addEventListener("readystatechange", function () {
                if (this.readyState === 4) {
                console.log(this.responseText);
                var myArr = JSON.parse(this.responseText);
                if(myArr.data.id != null && myArr.data.id != ""){
                   localStorage.setItem('MainId', myArr.data.id);
                    $('[notf="1"]').css("display", "inline");
                    setInterval(function(){ $('[notf="1"]').css({ "display": "none"}); }, 3000);
                }else{
                        $('[notf="0"]').css("display", "inline");
                    setInterval(function(){ $('[notf="0"]').css({ "display": "none"}); }, 3000);
                }
                }
                });
                xhr.open("POST", "http://localhost/exam/service.php?function=createMainQuestion");
                xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
                xhr.send(data);
               }

               function createQuestions(qid,qu,li,si,dv,ci,a1,a2,a3,a4,at){
                var data = "mainQuestion="+qid+"&question=" +qu+"&levelId="+ li+"&sectionId="+si+"&degreeValue="+dv+"&nos=done&creatorId="+ci+
                "&ans1="+a1+"&ans2="+a2+"&ans3="+a3+"&ans4="+a4+"&true=" + at;
                console.log(data);
                var xhr = new XMLHttpRequest();
                xhr.withCredentials = true;
                xhr.addEventListener("readystatechange", function () {
                if (this.readyState === 4) {
                console.log(this.responseText);
                var data = JSON.parse(this.responseText);
                if(data.data['e'] == 0){
                        $('[notf="1"]').css("display", "inline");
                    setInterval(function(){ $('[notf="1"]').css({ "display": "none"}); }, 3000);
                }else{
                        $('[notf="0"]').css("display", "inline");
                    setInterval(function(){ $('[notf="0"]').css({ "display": "none"}); }, 3000);
                }

               
                 setInterval(function(){ $('[not="0"]').css({ "display": "none"}); }, 3000);
                }
                });
                xhr.open("POST", "http://localhost/exam/service.php?function=createQuestions");
                xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
                xhr.send(data);
                } 

                       $('[save="1"]').on('click', function(){
                        
                        var val = $('[act="0"]').val();
                        if(val == 1){
                            createMainQuestions(3);
                        }else if(val == 2){
                          for(var i=0;i<4;i++){
                                var qid = $(`[op="0"]`).val();
                                var qu = $(`[q="`+i+`"]`).val();
                                var li = $(`[lev="0"]`).val();
                                var si = 3;
                                var dv = $(`[dg="`+i+`"]`).val();
                                var ci = localStorage.getItem('userId');
                                var a1 = $(`[anss="`+i+`"]`).val();
                                var a2 = $(`[anss1="`+i+`"]`).val();
                                var a3 = $(`[anss2="`+i+`"]`).val();
                                var a4 = $(`[anss3="`+i+`"]`).val();
                                var at = $(`[q="`+i+`"]`).val();
                                var ext = i == 0? '' : i;
                                var at = $('input[name="t' + ext + '"]:checked').val();
                                console.log("t" , i + 1, at);
                                if(qid != "" && qu != "" && li != "" && si != "" && dv != "" && ci != "" && a1 != "" && a2 != "" && a3 != "" && a4 != "" && at != undefined){
                                 createQuestions(qid,qu,li,si,dv,ci,a1,a2,a3,a4,at);
                                }    
                          }
                        }else if(val == 3){
                                for(var i=0;i<4;i++){
                                var qid = $(`[op="0"]`).val();
                                var qu = $(`[q="`+i+`"]`).val();
                                var li = $(`[lev="0"]`).val();
                                var si = 4;
                                var dv = $(`[dg="`+i+`"]`).val();
                                var ci = localStorage.getItem('userId');
                                var a1 = $(`[anss="`+i+`"]`).val();
                                var a2 = $(`[anss1="`+i+`"]`).val();
                                var a3 = $(`[anss2="`+i+`"]`).val();
                                var a4 = $(`[anss3="`+i+`"]`).val();
                                var at = $(`[q="`+i+`"]`).val();
                                var ext = i == 0? '' : i;
                                var at = $('input[name="t' + ext + '"]:checked').val();
                                console.log("t" , i + 1, at);
                                if(qid != "" && qu != "" && li != "" && si != "" && dv != "" && ci != "" && a1 != "" && a2 != "" && a3 != "" && a4 != "" && at != undefined){
                                 createQuestions(qid,qu,li,si,dv,ci,a1,a2,a3,a4,at);
                                } 
                                }
                        }else if(val == 4){
                                for(var i=0;i<4;i++){
                                var qid = 1;
                                var qu = $(`[q="`+i+`"]`).val();
                                var li = $(`[lev="0"]`).val();
                                var si = 1;
                                var dv = $(`[dg="`+i+`"]`).val();
                                var ci = localStorage.getItem('userId');
                                var a1 = $(`[anss="`+i+`"]`).val();
                                var a2 = $(`[anss1="`+i+`"]`).val();
                                var a3 = $(`[anss2="`+i+`"]`).val();
                                var a4 = $(`[anss3="`+i+`"]`).val();
                                var at = $(`[q="`+i+`"]`).val();
                                var ext = i == 0? '' : i;
                                var at = $('input[name="t' + ext + '"]:checked').val();
                                console.log("t" , i + 1, at);
                                if(qid != "" && qu != "" && li != "" && si != "" && dv != "" && ci != "" && a1 != "" && a2 != "" && a3 != "" && a4 != "" && at != undefined){
                                 createQuestions(qid,qu,li,si,dv,ci,a1,a2,a3,a4,at);
                                } 
                               }
                        }else if(val == 5){
                                for(var i=0;i<4;i++){
                                var qid = 1;
                                var qu = $(`[q="`+i+`"]`).val();
                                var li = $(`[lev="0"]`).val();
                                var si = 2;
                                var dv = $(`[dg="`+i+`"]`).val();
                                var ci = localStorage.getItem('userId');
                                var a1 = $(`[anss="`+i+`"]`).val();
                                var a2 = $(`[anss1="`+i+`"]`).val();
                                var a3 = $(`[anss2="`+i+`"]`).val();
                                var a4 = $(`[anss3="`+i+`"]`).val();
                                var at = $(`[q="`+i+`"]`).val();
                                var ext = i == 0? '' : i;
                                var at = $('input[name="t' + ext + '"]:checked').val();
                                console.log("t" , i + 1, at);
                                if(qid != "" && qu != "" && li != "" && si != "" && dv != "" && ci != "" && a1 != "" && a2 != "" && a3 != "" && a4 != "" && at != undefined){
                                 createQuestions(qid,qu,li,si,dv,ci,a1,a2,a3,a4,at);
                                } 
                                }
                        }else{

                        }
                       });
                    console.log( localStorage.getItem('userId'));

                        $('#save').click(function(){
        var check = true;
        var res = 0;
        $("input:checked").each(function(){
            var name = $(this).attr("name");
            var price = $('input[name="t3"]:checked').val();
                        alert(price);
             console.log($( this ).val());
             if($( this ).val() == 1){
                res++;
             }
        });
       // console.log(res);
        localStorage.setItem("voc", res);
        
    });
                       
        </script>
</body>

</html>
