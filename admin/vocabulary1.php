<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="assets/css/pace.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Progress Bars</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header text-center">
                    <h1 style="color:#fff"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
                  </div>
    <!-- /.navbar-right -->
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
        <aside class="site-sidebar clearfix">
                <nav class="sidebar-nav">
                        <ul class="nav in side-menu">
                          <li class="menu-item-has-children"><a href="vocabulary.html"><span class="color-color-scheme"><i class="list-icon material-icons">mail_outline</i> <span class="hide-menu">Vocabulary</span></span></a>

                          </li>
                            <li class="menu-item-has-children"><a href="grammer.html"><span class=""><i class="list-icon material-icons">mail_outline</i> <span class="hide-menu">Grammer</span></span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="listening.html"><i class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Listening</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="reading.html"><i class="list-icon material-icons">tune</i> <span class="hide-menu">Reading</span></a>

                            </li>
                        </ul>
                        <!-- /.side-menu -->
                    </nav>
            <!-- /.sidebar-nav -->
        </aside>
        <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->

                <!-- /.page-title-right -->

            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">

                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg" style="width: 60%;margin-left: 20%;">
                            <div class="widget-body clearfix">
                                <div class="row" >

                            <div class="col-md-12 widget-holder">
                                <form action="#" method="post">

                             

                                    <div class="col-md-12 mr-b-50" tb-data-jq="0">
                                        <h1 class="">vocablury</h1>
                                        <?php
                                         $array = array();
                                         $array1 = array();
                                         $json = getQuestions();
                                         $result = json_decode($json, true);
                                        // print_r($result['data']);
                                         $len =  sizeof($result['data']);
                                         for($i =0 ;$i<$len;$i++){
                                            $array1[] = $result['data'][$i]['id'];
                                        ?>
                                          <hr>
                                          <label class="btn btn-info pull-right"><?php echo $result['data'][$i]['degree_value'];  ?> Degrees</label>
                                          <h4><span><?php echo $i + 1;  ?></span><span>.</span> <?php echo $result['data'][$i]['question'];  ?></h4>
                                          <div class="form-group" ans='2'>
                                           <?php
                                              $json1 = getAnswers($result['data'][$i]['id']);
                                              $result1 = json_decode($json1, true);
                                             // print_r($result1['data'][0]);
                                              $lenn =  sizeof($result1['data']);
                                              for($ii =0 ;$ii<$lenn;$ii++){
                                                  if($result1['data'][$ii]['answer_true']== 1){
                                                    $array[] = $result1['data'][$ii]['id'];
                                                  }
                                           ?>
                                                <div class="radiobox radio-info">
                                                   <label>
                                                      <input type="radio" name=<?php echo $result['data'][$i]['id'];  ?> value= <?php echo $result1['data'][$ii]['id']; ?>> 
                                                      <span class="label-text"><?php echo $result1['data'][$ii]['answer'];  ?></span>
                                                   </label>
                                               </div>
                                           <?php
                                             }
                                           ?>
                                          </div>
                                        <?php
                                         }
                                       //  print_r($array);
                                        // print_r($array1); 
                                        ?>
                                          <script>
                                          localStorage.setItem('arr', <?php echo $array1; ?>);
                                          </script>
                                    </div>
                                        <input class="btn btn-success ripple" name="save" type="submit" value="save">
                                    
                                    <!-- /.col-md-4 -->
                                    <!-- Vertical Progress Bars -->

                                    <!-- /.col-md-3 -->
                                </form>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
    </div>
    <!-- /.content-wrapper -->
    <!-- FOOTER -->
    <footer class="footer text-center clearfix">2017 © Oscar Admin brought to you by UnifatoThemes</footer>
    </div>
    <?php
     function getQuestions(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://localhost/exam/service.php?function=getQuestions",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "levelId=1&sectionId=1&limit=3&offset=0",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          return "error";
        } else {
          return $response;
        }
     }

     function getAnswers($id){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://localhost/exam/service.php?function=getAnswers",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "questionId=".$id."&sectionId=1",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          return "error";
        } else {
          return $response;
        }
     }

     function getResults($array1){
         $r = 0;
        for($j = 0;$j<sizeof($array1);$j++){
           // echo $array1[$j];
        }
      }
      getResults($array1); 
      if(isset($_POST['save'])){
        for($j = 0;$j<sizeof($array1);$j++){
         echo $array1[$j];
         }
        
    }
    ?>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/custom.js"></script>

    <script>
    /*var data = "levelId=1&sectionId=1&limit=3&offset=0";
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener("readystatechange", function () {
     if (this.readyState === 4) {
       // $('[tb-data-jq="0"]').html('');
       var myArr = JSON.parse(this.responseText);
       console.log(myArr.data);
       for (var i = 0;i<myArr.data.length;i++){
       var temp = `<hr>
                                        <label class="btn btn-info pull-right">`+ myArr.data[i].degree_value +` Degrees</label>
                                        <h4><span>`+ (i + 1) +`</span><span>.</span> `+ myArr.data[i].question +`</h4>
                                        <div class="form-group" ans=`+ myArr.data[i].id +`>

                                        </div>`;
        $('[tb-data-jq="0"]').append(temp);
        getAnswers(myArr.data[i].id);
       }
    //   console.log(this.responseText);
    }
    });
    xhr.open("POST", "http://localhost/exam/service.php?function=getQuestions");
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhr.send(data);
    function getAnswers(id){
        var data = "questionId="+ id + "&sectionId=1";
        console.log(data);
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr.data);
            for(var i = 0;i < myArr.data.length;i++){
                var temp = `
                <div class="radiobox radio-info">
                    <label>
                        <input type="radio" name="`+ id +`" value="`+ myArr.data[i].answer_true +`"> <span class="label-text">`+ myArr.data[i].answer +`</span>
                    </label>
                </div>`;
                $(`[ans="`+ id +`"]`).append(temp);
            }
        }
       });
       xhr.open("POST", "http://localhost/exam/service.php?function=getAnswers");
       xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
       xhr.send(data);
    }
    //getAnswers(7);*/

    </script>
</body>

</html>
