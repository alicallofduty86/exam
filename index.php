<!DOCTYPE html>
<html lang="en">

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <style>
            .fa {
             margin-left: 6px;
             margin-right: 3px;
            
            }
        </style>
</head>

<body class="body-bg-full profile-page" style="background-image: url(assets/demo/night.jpg)">
    <div id="wrapper" class="row wrapper"><br><br><br>
        <div class="col-10 ml-sm-auto col-sm-6 col-md-4 ml-md-auto login-center mx-auto">
            <div class="navbar-header text-center">
              <h1 style="color:#51d2b7"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
            </div>
            <!-- /.navbar-header -->
            <form class="form-material">
                <div class="form-group">
                    <input type="email" placeholder="ali@enjaz.tech" class="form-control form-control-line" em="0" name="example-email" id="example-email">
                    <label for="example-email">Email</label>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="password" ps="0" class="form-control form-control-line">
                    <label>Password</label>
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-lg btn-color-scheme ripple" type="button"  confirm="0" id="login-btn">Login</button>
                </div>
                <div class="form-group no-gutters mb-0">
                  <!--  <div class="col-md-12 d-flex">
                        <div class="checkbox checkbox-info mr-auto">
                            <label class="d-flex">
                                <input type="checkbox"> <span class="label-text">Remember me</span>
                            </label>
                        </div><a href="javascript:void(0)" id="to-recover" class="my-auto pb-2 text-right"><i class="fa fa-lock mr-1"></i>Forgot Password?</a>
                    </div> -->
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.form-group -->
            </form>
            <div class="swal2-container swal2-fade swal2-shown" style="overflow-y: auto;display:none;padding-top: 11%;" notf="0">
            <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show" tabindex="-1" style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 345px;">
            <ul class="swal2-progresssteps" style="display: none;">
            </ul>
            <div class="swal2-icon swal2-error" style="display: none;">
            <span class="swal2-x-mark">
            <span class="swal2-x-mark-line-left">
            </span>
            <span class="swal2-x-mark-line-right">
            </span>
            </span>
            </div>
            <div class="swal2-icon swal2-question" style="display: none;">?</div>
            <div class="swal2-icon swal2-warning" style="display: none;">!</div>
            <div class="swal2-icon swal2-info" style="display: none;">i</div>
            <div class="swal2-icon swal2-error swal2-animate-error-icon" style="display: block;">
            <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255);">
            </div>
            <span class="swal2-x-mark swal2-animate-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>
             <div class="swal2-success-ring"></div> 
             <div class="swal2-success-fix" style="background: rgb(255, 255, 255);"></div>
             <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255);"></div>
             </div><img class="swal2-image" style="display: none;"><h2 class="swal2-title" id="swal2-title">Information Not Valid!</h2>
             <div id="swal2-content" class="swal2-content" style="display: block;">check your information .</div>
             </div></div>
            <!-- /.form-material -->
 
            <!-- /.btn-list -->
            <footer class="col-sm-12 text-center">
                <hr>
                <p>Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                </p>
            </footer>
        </div>
        <!-- /.login-center -->
    </div>
    <!-- /.body-container -->
    <!-- Scripts -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/material-design.js"></script>
    <script>
        $('#login-btn').on('click', function(){
            $('[confirm="0"]').append( "<i class='fa fa-refresh fa-spin fa-x fa-fw'></i>" );
            var data = {
                'email':$('[em="0"]').val(),
                'password' : $('[ps="0"]').val()
            }
            console.log(data);
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost/exam/service.php?function=login",
                "method": "POST",
                "headers": {
                    "content-type": "application/x-www-form-urlencoded"
                },
                'data' : data
            }
            $.ajax(settings).done(function(res){
                console.log(res);
                if(res.data != null){
                if (res.Result != 'done') {
                    
                    //alert('sign in true')
                }else {
                    
                    localStorage.setItem('userId', res.data.id);
                    console.log(res.data.level_id);
                    if(res.data.level_id === "1") {
                        console.log('res.data.level_id');
                        window.location.replace("http://localhost/exam/results.php");
                    }else if(res.data.level_id === "2") {
                        console.log('res.data.level_idggggggggggggg');
                        window.location.replace("http://localhost/exam/selectLevel.php");

                    }else{
                     $('[notf="0"]').css("display", "inline");
                     
                     
                    }
                }
                }else{
                    $('[notf="0"]').css("display", "inline");
                   
                    
                }
                $('.fa-spin').remove();
                setInterval(function(){ $('[notf="0"]').css({ "display": "none"}); }, 3000);
            })
        })
            // function login() {
            //     $('[confirm="0"]').append( "<i class='fa fa-refresh fa-spin fa-x fa-fw'></i>" );
            //     var ps = $('[ps="0"]').val();
            //     var em = $('[em="0"]').val();
            //     var data = "password=" + ps + "&" + "email=" + em;
            //     console.log(data);
            //     var xhr = new XMLHttpRequest();
            //     xhr.withCredentials = true;
            //     xhr.addEventListener("readystatechange", function() {
            //         //console.log(this.status );
            //         if (this.readyState === 4) {
            //             $('.fa-spin').remove();
            //             console.log(this.responseText);
            //            // localStorage.setItem("client_id",)
            //             if (this.responseText == 'true') {
            //                 window.location.replace("http://localhost/SelfaV6/index.php");
            //             } else {
                            
            //                 console.log('error');
            //             }
            //         } else {}
            //         console.log(this.responseText);
            //     });
            //     xhr.open("POST", "lib/api.php?req=login");
            //     xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            //     xhr.send(data);
            // }
        </script>
</body>

</html>