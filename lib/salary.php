<?php
class salary{
    private $Civilian = "http://portal.meits-co.net:7004/pensionAPI-pension-context-root/resources/GetPensionWS";
    private $Civilian_token = "404e9255-d1e9-getPension-QCard-f006ae0309bd";
    private $Association_id = 1;
    private $Pension_number = null;
    private $Military = "http://portal.meits-co.net:7004/pensionAPI-pension-context-root11/resources/GetPension1WS";
    private $Military_token = "404e9255-d1e9-getPension1-QCard-f006ae0309bd";
    private $CivilianW = "http://portal.meits-co.net:7004/pensionAPI-pension-context-root02/resources/GetPension2WS";
    private $CivilianW_token = "404e9255-d1e9-getPension2-QCard-f006ae0309bd";

    function __construct ($Pension_number){
     $this->Pension_number = $Pension_number;
    }

    public function getSalary(){
        $data =  $this->callService($this->Civilian,$this->Civilian_token,$this->Pension_number);
         if($data == false){
            $data =  $this->callService($this->Military,$this->Military_token,$this->Pension_number);
            if($data == false){
                $data =  $this->callService($this->CivilianW,$this->CivilianW_token,$this->Pension_number);
                return $data;
            }else{
               return $data;
            }
         }else{
            return $data;
         }
    }

    function callService($url,$token,$Pension_number){
        $curl = curl_init();
        $req = array();
        $req['key'] = $token;
        $req['pension_number'] = $Pension_number;
        $req['association_id'] = $this->Association_id;
        $data = json_encode($req);
        curl_setopt_array($curl, array(
          CURLOPT_PORT => "7004",
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          return false;
        } else {
            $resp = json_decode($response,true);
            if(empty($resp['code']))
            return $resp[0]['pensionSalary'];
            else
            return false;
        }
    }

}

function test($smtNo,$deptNo){
  $data = "smtNo=" . $smtNo ."&" ."deptNo=" . $deptNo;
  echo $data;
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://aqsati.net/jamyat/webService/get-bills-timeline.php",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "token: T1>K57V3?;-FDuqFOTrO}58+a`!|i|P_O0UD,Yn2Y!%Pz1^{L%Ky|xuc`Z6LA>;"
      ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
        $resp = json_decode($response,true);
        echo $response;
        foreach($resp as $b){
            echo $b['Date'];
            echo '<br>';
            echo $b['Amount'];
        }
    }
}

test(gmp_strval(gmp_init(1111111111111111)),gmp_strval(gmp_init(1111111111)));
//echo "/////////////////////////////////////////////////////////////////////////";
//$obj = new salary(3328136011);
//echo $obj->getSalary();