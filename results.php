<?php
require_once './lib/checkAdmin.php';
?>
<html lang="en">

<head>
    <link rel="stylesheet" href="assets/css/pace.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>View Results</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

</head>
<style>
    .questions {
        display: none;
    }

    #addQ {
        margin-bottom: 20px;
    }
    th,td{
        text-align:center;
    }
</style>
<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header text-center">
                    <h1 style="color:#fff"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
                  </div>
    <!-- /.navbar-right -->
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
        <aside class="site-sidebar clearfix">
                <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                          <li class=" menu-item-has-children"><a href="addQuestions.php"><span class="color-color-scheme"></span><i class="list-icon material-icons">playlist_add</i> <span class="hide-menu">Add Questions <span class="badge badge-border bg-primary pull-right">5</span></span></span></a>

                            </li>
                            <li class="current-page menu-item-has-children"><a href="results.php"><i class="list-icon material-icons">view_headline</i> <span class="hide-menu">View Results</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">contact_mail</i> <span class="hide-menu">Contact</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">info</i> <span class="hide-menu">About Us</span></a>

                            </li>
                            <li class="menu-item-has-children"><a href="./lib/logout.php"><i class="list-icon material-icons">exit_to_app</i> <span class="hide-menu">LogOut</span></a>

                            </li>
                        </ul>
                        <!-- /.side-menu -->
                    </nav>
            <!-- /.sidebar-nav -->
        </aside>
        <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <h1 class="text-center color-color-scheme">Members Results</h1>

            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row" content="0">
                    
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body">

                                <div class="fixed-table-body" style="padding:20px;">
                                <div class="form-group input-has-value">
                                        <div class="input-group input-has-value">
                                            <button class=" btn btn-warning ripple"   search="0">Search</button>
                                            <lable style="width:30px;" ></lable>
                                        <input type="text" id="datepicker" class="form-control " dt="0" >
                                        </div>
                                        <!-- /.input-group -->
                                    </div>
                                        <br>
                                    <div class="fixed-table-loading" style="top: 42px; display: none;">
                                        Loading, please wait...
                                    </div>
                                    <table style="border:1px #ccc solid;" table="0"  class="table table-striped table-hover" data-toggle="table" data-url="assets/js/bootstrap-table/bootstrap-table.json">
                                    <thead>
                                        <tr>
                                            <th style="" data-field="name" tabindex="0">
                                                <div class="th-inner ">Name</div><div class="fht-cell">

                                                </div>
                                            </th>
                                            <th style="" data-field="stargazers_count" tabindex="0">
                                                <div class="th-inner ">Department</div>
                                                
                                            </th>
                                            <th style="" data-field="forks_count" tabindex="0">
                                                <div class="th-inner ">Level</div>
                                                
                                            </th>
                                                <th style="" data-field="description" tabindex="0">
                                                    <div class="th-inner ">Score</div>
                                                    
                                                </th>
                                                <th style="" data-field="description" tabindex="0">
                                                    <div class="th-inner ">Status</div>
                                                    
                                                </th>
                                            </tr>
                                        </thead>
                                    <tbody rst="0">
    
                                    </tbody>
                                </table>
                                </div>
                                <button class=" btn btn-warning ripple"  id="csv" csv="0">Export Result</button>
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.col-md-12 -->
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
    </div>
    <!-- /.content-wrapper -->
    <!-- FOOTER -->
    <footer class="footer text-center clearfix">2017 © Oscar Admin brought to you by UnifatoThemes</footer>
    </div>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/themes/inlite/theme.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.4/jquery.tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.25/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>

    <script>
         getResults();
        $( function() {
            $( "#datepicker" ).datepicker({
                                            dateFormat: 'yy-mm-dd',//check change
                                            changeMonth: true,
                                            changeYear: true
                                            });
        } );
        $('[search="0"]').on('click',function(){
            $(`[rst="0"]`).html(``);
            getResults();
        })
        function getResults(){
        var dt = $('[dt="0"]').val();
        if(dt != '' && dt != undefined){
            var data = "date=" + dt;
        }else{
            var data = "date=" + getDate();
        }
        console.log(data);
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            console.log(this.responseText);
            var myArr = JSON.parse(this.responseText);
            console.log(myArr.data);
            if(myArr.data != "" && myArr.data != null){
                for(var i = 0;i < myArr.data.length;i++){
                var temp = `
                            <tr id="tr-id-1" class="tr-class-2" data-index="1">
                                 <td id="td-id-1" class="td-class-1" style="">`+myArr.data[i].name+`</td>
                                 <td style="">`+myArr.data[i].department_name+`</td>
                                 <td style="">`+myArr.data[i].level_name+`</td>
                                 <td style="">`+myArr.data[i].degree+`</td>
                                 <td style="">`;
                                 if(myArr.data[i].degree > 70){
                                 temp += ` <button class="btn btn-success btn-rounded ripple"><i class="material-icons list-icon">check</i>  <span>Success</span>
                                 </button>
                                 </td>
                             </tr>`;
                                 }else{
                                    temp += ` <button class="btn btn-danger btn-rounded ripple"><i class="material-icons list-icon">check</i>  <span>failed</span>
                                 </button>
                                 </td>
                             </tr>`;
                                 }
                $(`[rst="0"]`).append(temp);
            }
            }else{
                
                var temp = `
                            <tr id="tr-id-1" class="tr-class-2" data-index="1">
                             <td colspan="5"> No Results !!!!<td>
                             </tr>`;
                $(`[rst="0"]`).append(temp);
            }
        }
       });
       xhr.open("POST", "http://localhost/exam/service.php?function=getResult");
       xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
       xhr.send(data);
    }
    getDate();
    function getDate(){
        let date = new Date();
        let month
        let day_month
        if ((date.getMonth() +1 ) < 10) {
            month = '0' + (date.getMonth() +1 )
        }else {
            month = (date.getMonth() +1 )
        }
        if (date.getDate() < 10) {
            day_month = '0' + date.getDate()
        } else {
            day_month = date.getDate()
        }
        let day = date.getFullYear() + '-' + month + '-' + day_month
        console.log(day)
        $('[dt="0"]').val(day);
        return day;
     }

     /////////////////// print csv  ///////////////

     $(document).ready(function() {

        function exportTableToCSV($table, filename) {
            console.log($table);
        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function(i, row) {
            var $row = $(row),
                $cols = $row.find('td');

            return $cols.map(function(j, col) {
                var $col = $(col),
                text = $col.text();

                return text.replace(/"/g, '""'); // escape double quotes

            }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"';

        // Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

            var blob = new Blob([decodeURIComponent(csv)], {
            type: 'text/csv;charset=utf8'
            });

            // Crashes in IE 10, IE 11 and Microsoft Edge
            // See MS Edge Issue #10396033
            // Hence, the deliberate 'false'
            // This is here just for completeness
            // Remove the 'false' at your own risk
            window.navigator.msSaveBlob(blob, filename);

        } else if (window.Blob && window.URL) {
            // HTML5 Blob        
            var blob = new Blob([csv], {
            type: 'text/csv;charset=utf-8'
            });
            var csvUrl = URL.createObjectURL(blob);

            $(this)
            .attr({
                'download': filename,
                'href': csvUrl
            });
        } else {
            // Data URI
            var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
            .attr({
                'download': filename,
                'href': csvData,
                'target': '_blank'
            });
        }
        }

        // This must be a hyperlink
        
        $('[content="0"]').on('click','#csv', function(event) {
        // CSV
        console.log("dfgdfgdfgdfgdf");
        var args = [$('[table="0"]'), 'ali.csv'];

        exportTableToCSV.apply(this, args);

        // If CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
        });
        });
        </script>
</body>

</html>
