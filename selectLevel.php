<?php
require_once './lib/checkUser.php';
?>
<html lang="en">

<head>
    <link rel="stylesheet" href="assets/css/pace.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Cards</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header text-center">
                    <h1 style="color:#fff"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
                  </div>
    <!-- /.navbar-right -->
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
        <aside class="site-sidebar clearfix">
            <nav class="sidebar-nav">
            <ul class="nav in side-menu">
                    <li class="current-page menu-item-has-children"><a href="selectLevel.php"><span class="color-color-scheme"><i class="list-icon material-icons">home</i> <span class="hide-menu">Home <span class="badge badge-border bg-primary pull-right">5</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="javascript:void(0);"><span ><i class="list-icon material-icons">info</i> <span class="hide-menu">About Us</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">contact_mail</i> <span class="hide-menu">Contact</span></a>

                    </li>

                    <li class="menu-item-has-children"><a href="./lib/logout.php" out="0"><i class="list-icon material-icons" >exit_to_app</i> <span class="hide-menu">LogOut</span></a>

                    </li>
                </ul>
                <!-- /.side-menu -->
            </nav>
            <!-- /.sidebar-nav -->
        </aside>
        <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->

            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                    <br>
                <div class="row">
                    <!-- Simple Card -->

                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg-transparent">
                            <div class="widget-body clearfix">
                                <h3 class=" text-center" style="color:#51d2b7;">Select Examination Level</h3><br>
                                <div class="row">
                                    <div class="col-sm-3 mr-b-20">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title">Beginner Level</h5>
                                               
                                              
                                            </div>
                                            <div class="card-action"><a href="selectSection.php" class="btn btn-block btn-rounded btn-warning" select="0" >Start Examination</a>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 mr-b-20">
                                        <div class="card text-inverse bg-info">
                                            <div class="card-body">
                                                <h5 class="card-title">Introductory Level</h5>
                                                
                                            
                                            </div>
                                            <div class="card-action"><a href="#" class="btn btn-block btn-rounded btn-warning" select="3" >Start Examination</a>  
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-4 -->
                                    <div class="col-sm-3 mr-b-20">
                                        <div class="card text-inverse bg-color-scheme-dark">
                                            <div class="card-body">
                                                <h5 class="card-title">Intermediate Level</h5>
                                               
                                            </div>
                                            <div class="card-action"><a href="#" class="btn btn-block btn-rounded btn-warning" select="1">Start Examination</a> 
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-4 -->
                                    <div class="col-sm-3 mr-b-20">
                                        <div class="card text-inverse bg-success">
                                            <div class="card-body">
                                                <h5 class="card-title">Advance Level</h5>
                                                
                                               
                                            </div>
                                            <div class="card-action"><a href="" class="btn btn-block btn-rounded btn-warning" select="2">Start Examination</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-4 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
    </div>
    <!-- /.content-wrapper -->
    <!-- FOOTER -->
    <footer class="footer text-center clearfix">2017 © Oscar Admin brought to you by UnifatoThemes</footer>
    </div>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/custom.js"></script>
    <script>
    $(document).ready(function(){
    $('[select="0"]').click(function(){
      localStorage.setItem('levelId',1);
    });
     });
     $(document).ready(function(){
    $('[out="0"]').click(function(){
      localStorage.clear();
    });
     });
    </script>
</body>

</html>