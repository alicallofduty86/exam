<?php
require_once './lib/checkUser.php';
?>
<html lang="en">

<head>
    <link rel="stylesheet" href="assets/css/pace.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>select Section</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/weather-icons-master/weather-icons.min.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/weather-icons-master/weather-icons-wind.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header text-center">
                    <h1 style="color:#fff"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
                  </div>
    <!-- /.navbar-right -->
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
        <aside class="site-sidebar clearfix">
                <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                    <li class="current-page menu-item-has-children"><a href="selectLevel.php"><span class="color-color-scheme"><i class="list-icon material-icons">home</i> <span class="hide-menu">Home <span class="badge badge-border bg-primary pull-right">5</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="javascript:void(0);"><span ><i class="list-icon material-icons">info</i> <span class="hide-menu">About Us</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">contact_mail</i> <span class="hide-menu">Contact</span></a>

                    </li>

                    <li class="menu-item-has-children"><a href="./lib/logout.php"  out="0"><i class="list-icon material-icons" >exit_to_app</i> <span class="hide-menu">LogOut</span></a>

                    </li>
                </ul>
                        <!-- /.side-menu -->
                    </nav>
            <!-- /.sidebar-nav -->
        </aside>
        <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12">
                            <h3 class=" text-center" style="color:#51d2b7;">Select Examination Section</h3><br>
                    </div>
                    <!-- /.widget-list-title -->
                    <!-- Counter: Sales -->
                    <div class="col-md-3 col-sm-6 widget-holder">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body clearfix">
                                <div class="widget-counter">
                                    <h6>Vocabulary <small class="text-inverse">Section</small></h6>
                                    <h4 class=""><span class="counter">20</span><span>	&nbsp;Questions</span></h4><i class="material-icons">dns</i>
                                    <a href="vocabulary.php" class="btn btn-block btn-rounded btn-outline-default text-inverse" select="0">Start</a>
                                    <span class="btn btn-warning btn-rounded ripple btn-block" style="display:none;" select1="0">  <span>Finished</span>
                                    </span>
                                </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <!-- Counter: Subscriptions -->
                    <div class="col-md-3 col-sm-6 widget-holder">
                        <div class="widget-bg bg-color-scheme text-inverse">
                            <div class="widget-body clearfix">
                                    <div class="widget-counter">
                                            <h6>Grammar  <small class="text-inverse">Section</small></h6>
                                            <h4 class=""><span class="counter">15</span><span>	&nbsp;Questions</span></h4><i class="material-icons">dns</i>
                                            <a href="grammer.php" class="btn btn-block btn-rounded btn-outline-default text-inverse" select="1">Start</a>
                                            <span class="btn btn-warning btn-rounded ripple btn-block" style="display:none;" select1="1">  <span>Finished</span>
                                            </span>
                                        </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <!-- Counter: Users -->
                    <div class="col-md-3 col-sm-6 widget-holder">
                        <div class="widget-bg bg-info text-inverse">
                            <div class="widget-body clearfix">
                                    <div class="widget-counter">
                                            <h6>Reading  <small class="text-inverse">Section</small></h6>
                                            <h4 class=""><span class="counter">10</span><span>	&nbsp;Questions</span></h4><i class="material-icons">dns</i>
                                            <a href="reading.php" class="btn btn-block btn-rounded btn-outline-default text-inverse" select="2">Start</a>
                                            <span class="btn btn-warning btn-rounded ripple btn-block" style="display:none;" select1="2">  <span>Finished</span>
                                            </span>
                                        </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <!-- Counter: Pageviews -->
                    <div class="col-md-3 col-sm-6 widget-holder">
                        <div class="widget-bg">
                            <div class="widget-body clearfix">
                                    <div class="widget-counter">
                                            <h6>Listening  <small class="">Section</small></h6>
                                            <h4 class=""><span class="counter">10</span><span>	&nbsp;Questions</span></h4><i class="material-icons">dns</i>
                                            <a href="listening.php" class="btn btn-block btn-rounded btn-outline-info " select="3">Start</a>
                                            <span class="btn btn-warning btn-rounded ripple btn-block" style="display:none;" select1="3">  <span>Finished</span>
                                            </span>
                                        </div>
                                <!-- /.widget-counter -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <button class="btn btn-block btn-danger" fin="0">Finish The Test</button>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
    </div>
    <div class="swal2-container swal2-fade swal2-shown" style="overflow-y: auto;display:none;padding-top: 11%;" notf="0">
            <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show" tabindex="-1" style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 345px;">
            <ul class="swal2-progresssteps" style="display: none;">
            </ul>
            <div class="swal2-icon swal2-error" style="display: none;">
            <span class="swal2-x-mark">
            <span class="swal2-x-mark-line-left">
            </span>
            <span class="swal2-x-mark-line-right">
            </span>
            </span>
            </div>
            <div class="swal2-icon swal2-question" style="display: none;">?</div>
            <div class="swal2-icon swal2-warning" style="display: none;">!</div>
            <div class="swal2-icon swal2-info" style="display: none;">i</div>
            <div class="swal2-icon swal2-error swal2-animate-error-icon" style="display: block;">
            <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255);">
            </div>
            <span class="swal2-x-mark swal2-animate-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>
             <div class="swal2-success-ring"></div> 
             <div class="swal2-success-fix" style="background: rgb(255, 255, 255);"></div>
             <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255);"></div>
             </div><img class="swal2-image" style="display: none;"><h2 class="swal2-title" id="swal2-title">Error!!!</h2>
             <div id="swal2-content" class="swal2-content" style="display: block;">Wrong to send results !</div>
             </div></div>
    </div>
    <!-- /.content-wrapper -->
    <div class="swal2-container swal2-fade swal2-shown" style="overflow-y: auto;display:none;padding-top: 11%" notf="1">
    <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show" tabindex="-1" style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 345px;">
            <ul class="swal2-progresssteps" style="display: none;"></ul>
            <div class="swal2-icon swal2-error" style="display: none;">
                    <span class="swal2-x-mark"><span class="swal2-x-mark-line-left"></span>
                    <span class="swal2-x-mark-line-right"></span>
                </span>
        </div>
        <div class="swal2-icon swal2-question" style="display: none;">?</div>
        <div class="swal2-icon swal2-warning" style="display: none;">!</div
        ><div class="swal2-icon swal2-info" style="display: none;">i</div>
        <div class="swal2-icon swal2-success swal2-animate-success-icon" style="display: block;">
                <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255);"></div>
                <span class="swal2-success-line-tip swal2-animate-success-line-tip"></span> 
                <span class="swal2-success-line-long swal2-animate-success-line-long"></span>
                <div class="swal2-success-ring"></div> <div class="swal2-success-fix" style="background: rgb(255, 255, 255);"></div>
                <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255);"></div></div>
                <img class="swal2-image" style="display: none;">
                <h2 class="swal2-title" id="swal2-title">Done</h2>
                <div id="swal2-content" class="swal2-content" style="display: block;">Results sent successfully</div>
                <input class="swal2-input" style="display: none;">
                <input type="file" class="swal2-file" style="display: none;">
                <div class="swal2-range" style="display: none;"><output></output><input type="range"></div>
                <select class="swal2-select" style="display: none;"></select><div class="swal2-radio" style="display: none;"></div>
                <label for="swal2-checkbox" class="swal2-checkbox" style="display: none;">
                        <input type="checkbox"></label><textarea class="swal2-textarea" style="display: none;"></textarea>
                        <div class="swal2-validationerror" style="display: none;"></div>
                </div>
</div>
    <!-- /.content-wrapper -->
    <!-- FOOTER -->
    <footer class="footer text-center clearfix">2017 © Oscar Admin brought to you by UnifatoThemes</footer>
    </div>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <script src="assets/vendors/charts/utils.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-Knob/1.2.13/jquery.knob.min.js"></script>
    <script src="assets/vendors/charts/excanvas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mithril/1.1.1/mithril.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clndr/1.4.7/clndr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.time.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/custom.js"></script>
    <script>
     
     $(document).ready(function(){
    $('[select="0"]').click(function(){
        localStorage.setItem('sectionId',1);
    });
    $('[select="1"]').click(function(){
        localStorage.setItem('sectionId',2);
    });
    $('[select="2"]').click(function(){
        localStorage.setItem('sectionId',3);
    });
    $('[select="3"]').click(function(){
        localStorage.setItem('sectionId',4);
    });
     });

     $(document).ready(function(){
         if(localStorage.getItem("*")==="*"){
            $('[select="0"]').remove();
            $('[select1="0"]').css('display','block');
            
         }if(localStorage.getItem("**")==="**"){
            $('[select="1"]').remove();
            $('[select1="1"]').css('display','block');
            
         }if(localStorage.getItem("***")==="***"){
            $('[select="3"]').remove();
            $('[select1="3"]').css('display','block');
        
         }if(localStorage.getItem("****")==="****"){
            $('[select="2"]').remove();
            $('[select1="2"]').css('display','block');
            
         }
     })

        $('[fin="0"]').click(function(){
        var arr = '';
        if(localStorage.getItem("grm")!= undefined && localStorage.getItem("grm")!= '' && localStorage.getItem("voc")!= undefined && localStorage.getItem("voc")!= '' 
        && localStorage.getItem("rd1")!= undefined && localStorage.getItem("rd1")!= '' && localStorage.getItem("lis1")!= undefined && localStorage.getItem("lis1")!= ''
        && localStorage.getItem("rd2")!= undefined && localStorage.getItem("rd2")!= ''&& localStorage.getItem("lis2")!= undefined && localStorage.getItem("lis2")!= ''){
         
        arr = localStorage.getItem("grm") + ',' + localStorage.getItem("rd1")+ ',' + localStorage.getItem("rd2")+ ',' + localStorage.getItem("voc")+ ',' + localStorage.getItem("lis1")+ ',' + localStorage.getItem("lis2");
        console.log(arr);
        setScore(arr);
        }

    });
     function setScore(arr){
        var dt = getDate();
        var data = "memberId="+localStorage.getItem('userId')+"&testLevelId="+localStorage.getItem('levelId')+ "&testDate=" +dt+ "&array=" + arr;
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            console.log(this.responseText);
            var data = JSON.parse(this.responseText);
                if(data.data === true){
                 $('[notf="1"]').css("display", "inline");
                 setInterval(function(){ $('[notf="1"]').css({ "display": "none"}); }, 3000);
                }else{
                    $('[notf="0"]').css("display", "inline");
                    setInterval(function(){ $('[notf="0"]').css({ "display": "none"}); }, 3000);
                }
        }
        });
        xhr.open("POST", "http://localhost/exam/service.php?function=setMemberDegree");
        xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        xhr.send(data);
     }
     $(document).ready(function(){
    $('[out="0"]').click(function(){
      localStorage.clear();
    });
     });

     function getDate(){
        let date = new Date();
        let month
        let day_month
        if ((date.getMonth() +1 ) < 10) {
            month = '0' + (date.getMonth() +1 )
        }else {
            month = (date.getMonth() +1 )
        }
        if (date.getDate() < 10) {
            day_month = '0' + date.getDate()
        } else {
            day_month = date.getDate()
        }
        let day = date.getFullYear() + '-' + month + '-' + day_month
        console.log(day)
        return day;
     }
    </script>
</body>

</html>