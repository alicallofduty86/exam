<?php
class api{
    private $host = '127.0.0.1';
    private $port = '3306';
    private $db = 'exam';
    private $user = 'root';
    private $password = '';
    private $con = null;

    function __construct(){
        if($this->con == null){
            $this->con = mysqli_connect($this->host,$this->user,$this->password,$this->db,$this->port) or die ('error in connect');
        }
    }

    public function login($usr,$pas){
     $res = mysqli_query($this->con,"call login('$usr','$pas')");
     $myArray = array();
     if($res!=null){
     $row = mysqli_fetch_assoc($res) ;
    //echo $row['id'];
    //print_r( $myArray);
    return $row;
   }else{
       return null;
   }
    
    }

    public function register($nm,$em,$ps,$ph,$dt,$tk,$dp){
        $res = mysqli_query($this->con,"call register('$nm','$em','$ps','$ph','$dt','$tk','$dp')");
        if($res == true)
           return true;
        return false;

    }

    public function setMemberDegree($mi,$li,$td,$dg,$pa){
        $res = mysqli_query($this->con,"call setMemberDegree('$mi','$li','$td','$dg','$pa')");
        if($res == true)
           return true;
        return false;
    }

    public function getResult($dt){
        $res = mysqli_query($this->con,"call getResult('$dt')"); 
        $myArray = array();
        if($res!=null){
        while ($row = mysqli_fetch_assoc($res)){   
           $myArray [] = $row; 
       }
       return $myArray;
      }else{
          return null;
      }
       
       }

       public function createMainQuestion($ti,$mq,$id,$vId){
        $res = mysqli_query($this->con,"call createMainQuestion('$ti','$mq','$id','$vId')"); 
        if($res!=null){
        $row = mysqli_fetch_assoc($res);
       return $row;
      }else{
          return null;
      }
       }

       public function getAnswers($qi,$si){
        $res = mysqli_query($this->con,"call getAnswers('$qi','$si')"); 
        $myArray = array();
        if($res!=null){
        while ($row = mysqli_fetch_assoc($res)){   
           $myArray [] = $row; 
       }
       return $myArray;
      }else{
          return null;
      }
       }

       public function getQuestions($li,$si,$lm){
        $res = mysqli_query($this->con,"call getQuestions('$li','$si','$lm')"); 
        $myArray = array();
        if($res!=null){
        while ($row = mysqli_fetch_assoc($res)){   
           $myArray [] = $row; 
       }
       return $myArray;
      }else{
          return null;
      }
       }

       public function getQuestionsById($id,$lm){
        $res = mysqli_query($this->con,"call getQuestionsById('$id','$lm')"); 
        $myArray = array();
        if($res!=null){
        while ($row = mysqli_fetch_assoc($res)){   
           $myArray [] = $row; 
       }
       return $myArray;
      }else{
          return null;
      }
       }

       public function createQuestions($mq,$qu,$li,$si,$dv,$nos,$ci,$ans1,$ans2,$ans3,$ans4,$r){
        $res = mysqli_query($this->con,"call createQuestions('$mq','$qu','$li','$si','$dv','$nos','$ci','$ans1','$ans2','$ans3','$ans4','$r')"); 
        if($res!=null){
        $row = mysqli_fetch_assoc($res);
       return $row;
      }else{
          return null;
      }
       }

       public function getMainQuestions($id){
        $res = mysqli_query($this->con,"call getMainQuestions('$id')"); 
        $myArray = array();
        if($res!=null){
        while ($row = mysqli_fetch_assoc($res)){   
           $myArray [] = $row; 
        }
        return $myArray;
        }else{
          return null;
        }
       }

       public function setMemberQuestions($qi,$mi){
        $res = mysqli_query($this->con,"call setMemberQuestions('$qi','$mi')"); 
        return $res;
       }

       public function getRandMainQuestions($uId,$sid,$lv){
        $res = mysqli_query($this->con,"call getMainQuestionR('$uId','$sid','$lv')"); 
        $myArray = array();
        if($res!=null){

        while ($row = mysqli_fetch_assoc($res)){   
           $myArray [] = $row; 
           $id = $row['id'];
           //setMemberQuestions($row['id'],$uId);
         //  echo "call setMemberQuestions('$id','$uId')";
           //mysqli_query($this->con,"call setMemberQuestions('$id','$uId')"); 
        }
        return $myArray;
        }else{
          return null;
        }
       }

       public function getScore($arr){
        $res = mysqli_query($this->con,"call getScore('$arr')"); 
        $myArray = array();
        if($res!=null){
        $row = mysqli_fetch_assoc($res);   
        $score = $row['score'];
        return $score;
        }else{
          return null;
        }
       }



    public function response($status,$result,$data)
     {
	header("HTTP/1.1 $status 'ok'");
	$response['Result']=$result;
	//$response['status_message']=$status_message;
	$response['data']=$data;
	//$response['token']=$auth;
	$json_response=json_encode($response);
	return $json_response;
     }
}
header('Access-Control-Allow-Origin: *');  
header("Content-Type:application/json");
$method = $_SERVER['REQUEST_METHOD'];
if($method =='POST')
{
  if($_GET['function'] == 'login'){
    include_once './lib/session.php';
    $Session = new Session();
    $Session->init();
    $o = new api();
    if($_POST['email'] && $_POST['password']){
        $res = $o->login($_POST['email'],$_POST['password']);
        if(empty($res)){
            echo $o->response(200,'fail',$res);
        }else{
            $Session->set('id',$res['id']);
            if($res['level_id'] == 1){
                $Session->set('level','admin');
            }else{
                $Session->set('level','user');
            }
            echo $o->response(200,'done',$res);
        }
        
    }else{
        echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] =='register'){
    $o = new api();
    if($_POST['name'] && $_POST['email'] && $_POST['password'] && $_POST['phone'] && $_POST['craeteDate'] && $_POST['departmentId']){
    $res = $o->register($_POST['name'], $_POST['email'] , $_POST['password'] , $_POST['phone'] , $_POST['craeteDate'] , "alicall86" , $_POST['departmentId']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] =='createQuestions'){
    $o = new api();
    if($_POST['mainQuestion'] && $_POST['question'] && $_POST['levelId'] && $_POST['sectionId'] && $_POST['degreeValue'] && $_POST['nos']&&
    $_POST['creatorId'] && $_POST['ans1'] && $_POST['ans2'] && $_POST['ans3'] && $_POST['ans4'] && $_POST['true']){
    $res = $o->createQuestions($_POST['mainQuestion'] , $_POST['question'] , $_POST['levelId'] , $_POST['sectionId'] , $_POST['degreeValue'] , $_POST['nos'],
    $_POST['creatorId'] , $_POST['ans1'] , $_POST['ans2'] , $_POST['ans3'] , $_POST['ans4'] , $_POST['true']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'setMemberDegree'){
    $o = new api();
    if($_POST['memberId'] && $_POST['testLevelId'] && $_POST['testDate'] && $_POST['array']){
    $res = $o->getScore($_POST['array']);
    if($res>70){
     $pass = 1;
    }else{
        $pass = 0;
    }
    $o1 = new api();
    $res1 = $o1->setMemberDegree($_POST['memberId'] , $_POST['testLevelId'] , $_POST['testDate'] , $res , $pass);
    echo $o1->response(200,'done',$res1);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'getResult'){
    $o = new api();
    if($_POST['date']){
    $res = $o->getResult($_POST['date']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'createMainQuestion'){
    $o = new api();
    if($_POST['title'] && $_POST['MainQuestion'] && $_POST['sectionId'] && $_POST['levelId']){
    $res = $o->createMainQuestion($_POST['title'] , $_POST['MainQuestion'],$_POST['sectionId'],$_POST['levelId']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'getAnswers'){
    $o = new api();
    if($_POST['questionId'] && $_POST['sectionId']){
    $res = $o->getAnswers($_POST['questionId'] , $_POST['sectionId']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'getQuestions'){
    $o = new api();
    if($_POST['levelId'] && $_POST['sectionId'] && $_POST['limit']){
    $res = $o->getQuestions($_POST['levelId'] , $_POST['sectionId'] , $_POST['limit']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'getQuestionsById'){
    $o = new api();
    if($_POST['Id'] && $_POST['limit']){
    $res = $o->getQuestionsById($_POST['Id']  , $_POST['limit']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'getScore'){
    $o = new api();
    if($_POST['array']){
    $res = $o->getScore($_POST['array']);
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
  }elseif($_GET['function'] == 'getMainQuestions'){
    $o = new api();
    $res = $o->getMainQuestions($_POST['id']);
    echo $o->response(200,'done',$res);
  }elseif($_GET['function'] == 'getRandMainQuestions'){
    $o = new api();
    if($_POST['userId'] && $_POST['sectionId']&& $_POST['levelId']){
    $res = $o->getRandMainQuestions($_POST['userId'],$_POST['sectionId'],$_POST['levelId']);
    if(sizeof($res)==2){
    $o1 = new api();
    $res1 = $o1->setMemberQuestions($res[0]['id'],$_POST['userId']);
    $o2 = new api();
    $res2 = $o2->setMemberQuestions($res[1]['id'],$_POST['userId']);
    }elseif(sizeof($res)==1){
    $o1 = new api();
    $res1 = $o1->setMemberQuestions($res[0]['id'],$_POST['userId']);
    }else{
        
    }
    echo $o->response(200,'done',$res);
    }else{
    echo $o->response(200,'missing parameters',null);
    }
    }else{
    $bb=new api();
    $r=$bb->response(200,false,'select function','select function');
    echo $r;
  }
}else
{
       $bb=new api();
	   $r=$bb->response(200,false,'bad request','bad request');
	   echo $r;
}

?>