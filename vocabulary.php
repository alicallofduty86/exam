<?php
require_once './lib/checkUser.php';
?>
<html lang="en">

<head>
    <link rel="stylesheet" href="assets/css/pace.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/demo/favicon.png">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Progress Bars</title>
    <!-- CSS -->
    <link href="assets/vendors/material-icons/material-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/vendors/mono-social-icons/monosocialiconsfont.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Head Libs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="header-centered sidebar-horizontal">
    <div id="wrapper" class="wrapper">
        <!-- HEADER & TOP NAVIGATION -->
        <nav class="navbar">
            <!-- Logo Area -->
            <div class="navbar-header text-center">
                    <h1 style="color:#fff"><span style="color:#ffcc02 !important">Qi</span> Exam</h1>
                  </div>
    <!-- /.navbar-right -->
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
        <aside class="site-sidebar clearfix">
                <nav class="sidebar-nav">
                <ul class="nav in side-menu">
                    <li class="current-page menu-item-has-children"><a href="selectLevel.php"><span class="color-color-scheme"><i class="list-icon material-icons">home</i> <span class="hide-menu">Home <span class="badge badge-border bg-primary pull-right">5</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="selectSection.php"><span ><i class="list-icon material-icons">touch_app</i> <span class="hide-menu">Select Section</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="javascript:void(0);"><span ><i class="list-icon material-icons">info</i> <span class="hide-menu">About Us</span></span></a>

                    </li>
                    <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon material-icons">contact_mail</i> <span class="hide-menu">Contact</span></a>

                    </li>

                    <li class="menu-item-has-children"><a href="./lib/logout.php" out="0"><i class="list-icon material-icons">exit_to_app</i> <span class="hide-menu">LogOut</span></a>

                    </li>


                </ul>
                <div class="alert alert-icon alert-success border-success alert-dismissible fade show"  tim="0" style=" width: 10%;height: 47px;margin-top: -50px;    position: absolute;">   
   </div>
                        <!-- /.side-menu -->
                    </nav>
                  
        </aside>
        <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->

                <!-- /.page-title-right -->

            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="widget-list">
                <div class="row">
                    <div class="col-md-12 widget-holder">
                        <div class="widget-bg" style="width: 60%;margin-left: 20%;">
                            <div class="widget-body clearfix">
                              <div class="row" >

                                <div class="col-md-12 widget-holder">
                               

                             

                                    <div class="col-md-12 mr-b-50" tb-data-jq="0">
                                        <h1 class="">vocablury</h1>

                                        

                                    </div>
                                       
                                    <button class="btn btn-success ripple" id="save"><i class="material-icons list-icon">check</i>  <span>Save</span> 
                                 </button>
                                    
                                    <!-- /.col-md-4 -->
                                    <!-- Vertical Progress Bars -->
                                  </div>
                                    <!-- /.col-md-3 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.widget-list -->
        </main>
        <!-- /.main-wrappper -->
    </div>
    <div class="swal2-container swal2-fade swal2-shown" style="overflow-y: auto;display:none;padding-top: 11%;" notf="0">
                <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show" tabindex="-1" style="width: 500px; padding: 20px; background: rgb(255, 255, 255); display: block; min-height: 345px;">
                <ul class="swal2-progresssteps" style="display: none;">
                </ul>
                <div class="swal2-icon swal2-error" style="display: none;">
                <span class="swal2-x-mark">
                <span class="swal2-x-mark-line-left">
                </span>
                <span class="swal2-x-mark-line-right">
                </span>
                </span>
                </div>
                <div class="swal2-icon swal2-question" style="display: none;">?</div>
                <div class="swal2-icon swal2-warning" style="display: none;">!</div>
                <div class="swal2-icon swal2-info" style="display: none;">i</div>
                <div class="swal2-icon swal2-error swal2-animate-error-icon" style="display: block;">
                <div class="swal2-success-circular-line-left" style="background: rgb(255, 255, 255);">
                </div>
                <span class="swal2-x-mark swal2-animate-x-mark"><span class="swal2-x-mark-line-left"></span><span class="swal2-x-mark-line-right"></span></span>
                 <div class="swal2-success-ring"></div> 
                 <div class="swal2-success-fix" style="background: rgb(255, 255, 255);"></div>
                 <div class="swal2-success-circular-line-right" style="background: rgb(255, 255, 255);"></div>
                 </div><img class="swal2-image" style="display: none;"><h2 class="swal2-title" id="swal2-title">Answer all!</h2>
                 <div id="swal2-content" class="swal2-content" style="display: block;">Some questions have not been answered .</div>
                 </div></div>
    <!-- /.content-wrapper -->
    <!-- FOOTER -->
    <footer class="footer text-center clearfix">2017 © Oscar Admin brought to you by UnifatoThemes</footer>
    </div>
    <!--/ #wrapper -->
    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.2/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.1.3/mediaelementplayer.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.0/metisMenu.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.4/sweetalert2.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/custom.js"></script>

    <script>
    var data = "levelId="+localStorage.getItem("levelId")+"&sectionId=" +localStorage.getItem("sectionId")+ "&limit=20";
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.addEventListener("readystatechange", function () {
     if (this.readyState === 4) {
       // $('[tb-data-jq="0"]').html('');
       var myArr = JSON.parse(this.responseText);
       console.log(myArr.data);
       for (var i = 0;i<myArr.data.length;i++){
       var temp = `<hr>
                                        <label class="btn btn-info pull-right">`+ myArr.data[i].degree_value +` Degrees</label>
                                        <h4><span>`+ (i + 1) +`</span><span>.</span> `+ myArr.data[i].question +`</h4>
                                        <div class="form-group" ans=`+ myArr.data[i].id +`>

                                        </div>`;
        $('[tb-data-jq="0"]').append(temp);
        getAnswers(myArr.data[i].id);
       }
    //   console.log(this.responseText);
    }
    });
    xhr.open("POST", "http://localhost/exam/service.php?function=getQuestions");
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhr.send(data);
    function getAnswers(id){
        var data = "questionId="+ id + "&sectionId=" + localStorage.getItem("sectionId");
        console.log(data);
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr.data);
            for(var i = 0;i < myArr.data.length;i++){
                var temp = `
                <div class="radiobox radio-info">
                    <label>
                        <input type="radio" name="`+ id +`" value="`+ myArr.data[i].id+`"> <span class="label-text">`+ myArr.data[i].answer +`</span>
                    </label>
                </div>`;
                $(`[ans="`+ id +`"]`).append(temp);
            }
            counter(1);
        }
       });
       xhr.open("POST", "http://localhost/exam/service.php?function=getAnswers");
       xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
       xhr.send(data);
    }
    //getAnswers(7);
    /*$('#save').click(function(){
        var correctAns = $('#save').parent().find('div.form-group div.radiobox label input:checked').filter(function(){
            return this.name;
        })
        console.log(correctAns)

    })*/
    $('#save').click(function(){
        var check = true;
        var res = 0;
        var st = '';
        $("input:checked").each(function(){
            var name = $(this).attr("name");
            
             console.log($( this ).val());
              st += $( this ).val() + ',';
              res +=1;
        });
        if(res == 4){
        console.log(res , 'number');
        console.log(trim(st,','));
        localStorage.setItem("voc", trim(st,','));
        localStorage.setItem("*","*");
        location.replace('selectSection.php');
        }else{
            $('[notf="0"]').css("display", "inline");
             setInterval(function(){ $('[notf="0"]').css({ "display": "none"}); }, 4000);
        }
        
    });

    function trim (s, c) {
        if (c === "]") c = "\\]";
        if (c === "\\") c = "\\\\";
        return s.replace(new RegExp(
            "^[" + c + "]+|[" + c + "]+$", "g"
        ), "");
        }
        $(document).ready(function(){
    $('[out="0"]').click(function(){
      localStorage.clear();
    });
     });
        // counter
        function counter(min) {
            let input = min * 60  ;
            let remaining = input
            let counter = input;
            let timer;
            setInterval(function(){
                let minutes = Math.floor(remaining/60);
                if((input -remaining  === input)){
                    timer ='Done'
                    var check = true;
                    var res = 0;
                    var st = '';
                    $("input:checked").each(function(){
                        var name = $(this).attr("name");
                        
                        console.log($( this ).val());
                        st += $( this ).val() + ',';
                        res +=1;
                    });
                    console.log(res , 'number');
                    console.log(trim(st,','));
                    localStorage.setItem("voc", trim(st,','));
                    localStorage.setItem("*","*");
                    setInterval(function(){ location.replace('selectSection.php'); }, 2000);
                    
                } else {
                    if(Math.floor(remaining/60) < 10){
                      remain_min ='0'+ Math.floor(remaining/60)
                    }else {
                        let remain_min = Math.floor(remaining/60);
                    }
                    let remain_sec = remaining - (minutes*60) ;
                    timer = remain_min +': ' +remain_sec
                    console.log(remain_min, remain_sec)
                    remaining --;
                }
                $('[tim="0"]').text(timer);

            },1000)
        }
    </script>
</body>

</html>
